public class Cycle {
	
	private byte numOfWheels;
	private int	 weight;
	
	//default constructor, calls overloaded constructor
	public Cycle() {
		this((byte) 100, 1000);
	}
	
	//overloaded constructor
	public Cycle(byte numOfWheels, int weight) {
		this.numOfWheels = numOfWheels;
		this.weight = weight;
	}

	//returns a Cycle object in a string format
	public String toString() {
		return "Number of Wheels: " + this.numOfWheels + "\nWeight: " + this.weight;
	}
}
